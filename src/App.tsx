import * as React from 'react';

import './App.css';
import CodeEditor from "./components/code-editor/code-editor";
import Header from './components/header/header';

class App extends React.Component {

  public state = {
    value: 'console.log(\':)\') ...'
  };

  onChange = (value: string) => {
    this.setState({value})
  };

  public render() {
    const { value } = this.state;
    return (
      <div className='app'>
        <Header/>
        <div className="main-container">
          <div className="navigation-menu">

          </div>
          <div className="content">
            <CodeEditor value={value} onChange={this.onChange}/>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
